How to Setup Gunicorn and Ngnix on production. 

Step 1 : Install Gunicorn

pip install gunicorn

Step 2: Run Gunicorn

gunicorn --bind 0.0.0.0:8000 django_project.wsgi:application

where django_project is the name of the project

Note 1: 

If there is an error like "The SECRET_KEY setting must not be empty", then do following

gunicorn myproj.wsgi:application --env DJANGO_SETTINGS_MODULE='myproj.settings.production'

Also, note that, there's a following line in wsgi.py.

e.g. 

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_project.settings.production")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "videotube.settings.development")


Ref:

https://www.digitalocean.com/community/tutorials/how-to-deploy-python-wsgi-apps-using-gunicorn-http-server-behind-nginx

1) Run nginx

sudo nginx 

2) stop nginx 

nginx -s stop


