from apps.books.models.author import Author
from apps.books.models.book import Book


__all__ = ['Author', 'Book']
