from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        app_label = 'books'

    def __unicode__(self):
        return self.name