from django.db import models
from apps.books.models.author import Author


class Book(models.Model):
    name = models.CharField(max_length=255)
    pub_date = models.DateTimeField('date published')
    author = models.ForeignKey(Author)

    class Meta:
        app_label = 'books'

    def __unicode__(self):
        return self.name
