from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
                       url(r'^index', views.index, name='index'),
                       url(r'^authors$', views.author_list, name='author_list'),
                       url(r'^add_author/$', views.add_author, name='add_author'),
                       )
