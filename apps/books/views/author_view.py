from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from apps.books.models.author import Author
from apps.books.forms.author_form import AuthorForm


def author_list(request):
    all_authors = Author.objects.all()
    paginator = Paginator(all_authors, 3)  # Show 3 authors per page

    page = request.GET.get('page')

    try:
        authors = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        authors = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        authors = paginator.page(paginator.num_pages)

    return render(request, 'books/author_list.html', {"authors": authors})


def add_author(request):
    # A HTTP POST?

    if request.method == 'POST':
        form = AuthorForm(request.POST)

        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new Author to the database.
            form.save(commit=True)

            # Now call the index() view.
            # The user will be shown the author list page.
            # return author_list(request)
            return redirect('author_list')
        else:
            # The supplied form contained errors - just print them to the terminal.
            print form.errors
    else:
        # If the request was not a POST, display the form to enter details.
        form = AuthorForm()

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'books/author_form.html', {'form': form})
