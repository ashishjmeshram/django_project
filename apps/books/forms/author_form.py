from django import forms
from apps.books.models import Author


class AuthorForm(forms.ModelForm):
    name = forms.CharField(max_length=50, help_text="Please enter the author name.")

    # An inline class to provide additional information on the form.
    class Meta:
        # Provide an association between the ModelForm and a model
        model = Author
        fields = ('name',)
