from fabric.api import *
import os

# Hosts to deploy onto
env.hosts = ['localhost']

# Where your project code lives on the server
env.project_root = os.getcwd()

# Project Name
env.project_name = 'django_project'

# e.g. fab deploy_static_files:settings=production


def deploy_static_files(settings):
    command = './manage.py collectstatic -v0 --noinput --settings='+ env.project_name +'.settings.' + settings

    with cd(env.project_root):
        local(command)